function isFirst(nb) {
    var i = 2;
    while (i < nb) {
        if (nb % i === 0) {
            return false
        }
        i++
    }
    return true
}

function firsts(limit) {
    const tab = []
    for (let i = 0; i < limit; i++) {
        isFirst(i) && tab.push(i)
    }
    return tab
}

console.log(firsts(100000))