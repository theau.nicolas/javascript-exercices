/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : faire un appel API asynchrone                                  #
#                                                                              #
#                                                                              #
# *****************************************************************************/

import axios from "axios"

const getRestaurants = async () => {
    const res = await axios.get('https://frozen-reef-84613.herokuapp.com/api/restaurants')
    return (res.data.data)
}

const main = async () => {
    const res = await getRestaurants()
    console.log(res)
}

main()