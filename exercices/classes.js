/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : ecrire une classe qui décrit un élève                          #
#                                                                              #
#                                                                              #
# *****************************************************************************/


// class Car {
//     constructor(color, brand, immatriculation) {
//         this.color = color;
//         this.brand = brand;
//         this.immatriculation = immatriculation
//     }

//     getColor = () => {
//         return this.color;
//     }

//     setColor = (newColor) => {
//         this.color = newColor;
//     }
// }


// const spreadAdd = (notes) => {
//     notes = [...notes, 1]
// }

// const bmw = new Car('bleue', "BMW", "AZE345")
// bmw.setColor("rouge")
// console.log(bmw.getColor())

const average = (notes) => {
    return notes.reduce((a, v) => a + v) / notes.length
}

class Student {
    constructor(firstname, lastname, notes) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.notes = notes
    }

    getAverage = () => {
        return this.notes.reduce((a, v) => a + v) / this.notes.length
    }

    getFullname = () => {
        return (this.firstname + " " + this.lastname)
    }

    addNote = (note) => {
        this.notes.push(note)
    }

}

const student1 = new Student("John", "Doe", [1, 4, 6, 12, 13, 10])
console.log(student1.getAverage())
console.log(student1.addNote(20))
console.log(student1.getAverage())



