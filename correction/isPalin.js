/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : exercice sur les fonctions, dire si un mot est                 #
#    un palindrome ou non                                                      #
#                                                                              #
#                                                                              #
# *****************************************************************************/

function isPalin(src) {
    return (src === src.split('').reverse().join(''))
}

console.log(isPalin("kayak"))