class Ant {
    constructor(x, y, canvas) {
        this.x = x;
        this.y = y;
        this.direction = Math.floor(Math.random() * 4);
        this.state = "searching";
        this.pheromone = 0;
        this.ctx = canvas.getContext("2d");
        this.cells = [];
        this.numFood = 20;
        this.hasFood = false;
        this.foodX = 0;
        this.foodY = 0;
    }

    // Initialize the grid
    initCells() {
        for (var i = 0; i < 100; i++) {
            this.cells[i] = [];
            for (var j = 0; j < 100; j++) {
                this.cells[i][j] = { x: i, y: j, pheromone: 0, food: false };
            }
        }

        // Place food randomly on the grid
        for (var i = 0; i < this.numFood; i++) {
            var x = Math.floor(Math.random() * 100);
            var y = Math.floor(Math.random() * 100);
            this.cells[x][y].food = true;
        }
    }

    move() {
        switch (this.direction) {
            case 0: // move up
                this.y -= 1;
                break;
            case 1: // move right
                this.x += 1;
                break;
            case 2: // move down
                this.y += 1;
                break;
            case 3: // move left
                this.x -= 1;
                break;
        }

        // check if ant found food
        if (this.cells[this.x][this.y].food) {
            this.hasFood = true;
            this.foodX = this.x;
            this.foodY = this.y;
            this.cells[this.x][this.y].food = false;
        }
    }

    update() {
        switch (this.state) {
            case "searching":
                this.move();
                if (this.hasFood) {
                    this.state = "returning";
                }
                break;
            case "returning":
                this.move();
                this.depositPheromone();
                if (this.x === 50 && this.y === 50) {
                    this.state = "depositingFood";
                }
                break;
            case "depositingFood":
                this.hasFood = false;
                this.state = "searching";
                break;
        }
    }

    depositPheromone() {
        this.cells[this.x][this.y].pheromone += 1;
        this.pheromone -= 1;
    }

    draw() {
        // clear canvas
        this.ctx.clearRect(0, 0, canvas.width, canvas.height);

        // draw cells
        for (var i = 0; i < 100; i++) {
            for (var j = 0; j < 100; j++) {
                var cell = this.cells[i][j];
                if (cell.food) {
                    this.ctx.fillStyle = "yellow";
                    this.ctx.fillRect(i, j, 1, 1);
                } else if (cell.pheromone > 0) {
                    this.ctx.fillStyle = "blue";
                    this.ctx.fillRect(i, j, 1, 1);
                }
            }
        }

        // draw ants
        this.ctx.fillStyle = "pink";
        this.ctx.fillRect(this.x, this.y, 1, 1);
    }
}