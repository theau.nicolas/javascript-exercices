// Variables pour la taille du canvas
// var canvasWidth = 800;
// var canvasHeight = 600;

// // Tableau pour stocker la nourriture
// var food = [];

// // Tableau pour stocker les fourmis
// var ants = [];

// // Tableau pour stocker les traces de phéromones
// var pheromones = [];

// // Variables pour la position de la fourmilière
// var anthillX = canvasWidth / 2;
// var anthillY = canvasHeight / 2;

// // Initialisation du canvas
// var canvas = document.getElementById("ant-canvas");
// var ctx = canvas.getContext("2d");
// canvas.width = canvasWidth;
// canvas.height = canvasHeight;

// // Fonction pour générer les fourmis
// function generateAnts(numAnts) {
//     for (var i = 0; i < numAnts; i++) {
//         // Génération aléatoire de la position de départ de chaque fourmi
//         var antX = anthillX;
//         var antY = anthillY;

//         // Ajout de la fourmi au tableau
//         ants.push({ x: antX, y: antY, food: false });
//     }
// }

// // Fonction pour générer de la nourriture
// function generateFood(numFood) {
//     for (var i = 0; i < numFood; i++) {
//         // Génération aléatoire de la position de la nourriture
//         var foodX = Math.random() * canvasWidth;
//         var foodY = Math.random() * canvasHeight;

//         // Ajout de la nourriture au tableau
//         food.push({ x: foodX, y: foodY });
//     }
// }

// // Fonction pour mettre à jour la position des fourmis
// function updateAnts() {
//     for (var i = 0; i < ants.length; i++) {
//         var ant = ants[i];

//         // Si la fourmi a de la nourriture, elle retourne à la fourmilière
//         if (ant.food) {
//             ant.x = anthillX;
//             ant.y = anthillY;
//             ant.food = false;
//         } else {
//             // Sinon, la fourmi se déplace aléatoirement
//             ant.x += Math.random() * 2 - 1;
//             ant.y += Math.random() * 2 - 1;
//             // Limite de déplacement de la fourmi à la taille du canvas
//             ant.x = Math.max(0, Math.min(canvasWidth - 1, ant.x));
//             ant.y = Math.max(0, Math.min(canvasHeight - 1, ant.y));

//             // Vérifie si la fourmi croise de la nourriture
//             for (var j = 0; j < food.length; j++) {
//                 var pieceOfFood = food[j];
//                 var dx = pieceOfFood.x - ant.x;
//                 var dy = pieceOfFood.y - ant.y;
//                 var distance = Math.sqrt(dx * dx + dy * dy);

//                 if (distance < 1) {
//                     // La fourmi ramasse la nourriture et laisse une trace de phéromones
//                     ant.food = true;
//                     pheromones.push({ x: pieceOfFood.x, y: pieceOfFood.y });
//                     food.splice(j, 1);
//                 }
//             }
//         }
//     }
// }

// // Fonction pour dessiner les fourmis, les traces de phéromones et la nourriture sur le canvas
// function drawAnts() {
//     ctx.clearRect(0, 0, canvasWidth, canvasHeight);

//     // Dessin des traces de phéromones
//     ctx.fillStyle = "rgba(0, 0, 255, 0.1)";
//     for (var i = 0; i < pheromones.length; i++) {
//         ctx.fillRect(pheromones[i].x, pheromones[i].y, 1, 1);
//     }

//     // Dessin de la nourriture
//     ctx.fillStyle = "rgb(255, 255, 0)";
//     for (var i = 0; i < food.length; i++) {
//         ctx.beginPath();
//         ctx.arc(food[i].x, food[i].y, 2, 0, 2 * Math.PI);
//         ctx.fill();
//     }

//     // Dessin des fourmis
//     ctx.fillStyle = "rgba(255, 0, 255, 0.5)";
//     for (var i = 0; i < ants.length; i++) {
//         var ant = ants[i];
//         ctx.fillRect(ant.x, ant.y, 1, 1);
//     }
// }




// Appel de la fonction pour générer de la nourriture
// generateFood(200);


// // Appel de la fonction pour générer les fourmis
// generateAnts(1000);
const terrain = new Terrain(100, 100)

terrain.createTerrain()
terrain.testDiffusion()

// Boucle principale pour mettre à jour et dessiner les fourmis
setInterval(function () {
    // updateAnts();
    // drawAnts();
    terrain.updateTerrain()
    terrain.draw()
}, 500);
