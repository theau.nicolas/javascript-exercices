/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : comparer des variables entre elles                             #
#                                                                              #
#                                                                              #
# *****************************************************************************/

var a = 1;
var b = 2;
var c = 3;

if (a === b && b === c) {
    console.log("Les 3 variables sont identiques.");
} else if (b === c || a === c || a === b) {
    console.log("2 des variables sont de valeurs égales.");
} else {
    console.log("Les 3 variables sont différentes.");
}

