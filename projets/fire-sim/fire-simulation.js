// Variables pour la taille du canvas et le niveau de détail de la fractale
var canvasWidth = 500;
var canvasHeight = 500;
var fractalLevel = 10;
var distance = 3

// Tableau pour stocker les arbres
var trees = [];

// Initialisation du canvas
var canvas = document.getElementById("fire-canvas");
var ctx = canvas.getContext("2d");
canvas.width = canvasWidth;
canvas.height = canvasHeight;

// Fonction pour générer les arbres en utilisant une fractale
function generateTrees(x, y, width, height, level) {
    // Arrêt de la récursion lorsque le niveau de détail est atteint
    if (level === 0) {
        return;
    }

    // Calcul du nombre d'arbres à générer dans cette zone
    var numTrees = Math.random() * (width * height) / (1000 * level);

    // Boucle pour générer les arbres
    for (var i = 0; i < numTrees; i++) {
        // Génération aléatoire des positions des arbres
        var treeX = x + Math.random() * width;
        var treeY = y + Math.random() * height;

        // Ajout de l'arbre au tableau
        trees.push({ x: treeX, y: treeY, state: "alive" });
    }

    // Appel récursif de la fonction pour générer des arbres dans les sous-zones
    generateTrees(x, y, width / 2, height / 2, level - 1);
    generateTrees(x + width / 2, y, width / 2, height / 2, level - 1);
    generateTrees(x, y + height / 2, width / 2, height / 2, level - 1);
    generateTrees(x + width / 2, y + height / 2, width / 2, height / 2, level - 1);
}

// Appel de la fonction pour générer les arbres
generateTrees(0, 0, canvasWidth, canvasHeight, fractalLevel);

// Mettre l'arbre le plus au milieu en feu
var middleTree = trees[Math.floor(trees.length / 2)];
console.log(trees)
middleTree.state = "burning";

// Fonction pour mettre à jour l'état des arbres
function updateTrees() {
    for (var i = 0; i < trees.length; i++) {
        // Vérifie si un arbre est en train de brûler
        if (trees[i].state === "burning") {
            // Mise à jour de l'état de l'arbre en fonction de son état actuel
            if (trees[i].state === "burning") {
                trees[i].state = "burned";
            }

            // Contamination des arbres autour de l'arbre en feu
            for (var j = 0; j < trees.length; j++) {
                if (Math.abs(trees[i].x - trees[j].x) <= distance && Math.abs(trees[i].y - trees[j].y) <= distance) {
                    if (trees[j].state === "alive") {
                        trees[j].state = "burning";
                    }
                }
            }
        }
    }
}

// Fonction pour dessiner les arbres sur le canvas
function drawTrees() {
    ctx.fillStyle = "rgb(160,150,130)";
    ctx.fillRect(0, 0, canvasWidth, canvasHeight);

    for (var i = 0; i < trees.length; i++) {
        if (trees[i].state === "alive") {
            ctx.fillStyle = "green";
        } else if (trees[i].state === "burning") {
            ctx.fillStyle = "rgb(255, 165, 0)";
        } else if (trees[i].state === "burned") {
            ctx.fillStyle = "black";
        }
        ctx.fillRect(trees[i].x, trees[i].y, 1, 1);
    }
}



// Boucle principale pour mettre à jour et dessiner les arbres
setInterval(function () {
    updateTrees();
    drawTrees();
}, 1000);
