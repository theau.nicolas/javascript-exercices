/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : ecrire une classe qui décrit un élève                          #
#                                                                              #
#                                                                              #
# *****************************************************************************/

function average(notes) {
    return (notes.reduce((a, v) => a + v) / notes.length)
}

class Student {
    constructor(firstname, lastname, notes) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.notes = notes;
    }

    getFullname() {
        console.log(this.firstname + " " + this.lastname)
    }

    getAverage() {
        console.log(average(this.notes))
    }
}

const student1 = new Student("john", "doe", [1, 20, 13, 17, 4])
student1.getAverage()


