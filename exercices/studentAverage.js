/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice1 : faire la moyenne de la classe                                 #
#    Exercice2 : faire la moyenne pour chaque élève et ajouter le résultat     #
#                dans l'objet, puis afficher l'objet                           #
#    Exercice3 : Trier les élèves du meilleur au moins bon                     #
#    Exercice4 : Afficher le top 3 des élèves avec une petite phrase           #
#    Exercice5 : Ajouter puis afficher dans le podium la pire et la meilleure  #
#                note                                                          #
#                                                                              #
#                                                                              #
# *****************************************************************************/

const students = [
    {
        name: "John",
        note: [1, 20, 13, 17, 4],
    },
    {
        name: "Jane",
        note: [17, 13, 12, 10, 9]
    },
    {
        name: "Sophie",
        note: [17, 12, 14, 15, 14]
    },
    {
        name: "Marc",
        note: [2, 3, 5, 8, 9]
    },
    {
        name: "Manon",
        note: [18, 17, 18, 19, 12]
    },
    {
        name: "Jean",
        note: [18, 17, 18, 19, 12]
    }
]

const average = (tab) => {
    var total = 0
    var n = 0
    var i = 0;
    while (i < tab.length) {
        var j = 0;
        while (j < tab[i].note.length) {
            total = total + tab[i].note[j]
            n++;
            j++;
        }
        i++;
    }
    return (total / n)
}

const averagePerStudent = (tab) => {
    var i = 0;
    while (i < tab.length) {
        var total = 0
        var j = 0;
        while (j < tab[i].note.length) {
            total += tab[i].note[j]
            j++;
        }
        tab[i].average = total / j
        i++;
    }
    return (tab)
}

const sortByAverage = (tab) => {
    tab.sort((a, b) => b.average - a.average)
    return tab
}

const topThree = (tab) => {
    return tab.splice(0, 3)
}

const podium = (tab) => {
    return [
        "La première place est " + tab[0].name + " avec une moyenne de " + tab[0].average + ". Meilleure note : " + Math.max(...tab[0].note) + " | Pire note : " + Math.min(...tab[0].note),
        "La deuxième place est " + tab[1].name + " avec une moyenne de " + tab[1].average + ". Meilleure note : " + Math.max(...tab[1].note) + " | Pire note : " + Math.min(...tab[1].note),
        "La troisième place est " + tab[2].name + " avec une moyenne de " + tab[2].average + ". Meilleure note : " + Math.max(...tab[2].note) + " | Pire note : " + Math.min(...tab[2].note),
    ]
}

console.log(podium(sortByAverage(averagePerStudent(students))))

