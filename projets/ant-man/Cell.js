class Cell {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.pheromone = 0;
        this.hasFood = false;
        this.hasNest = false;
        this.evaporationRate = 10;
        this.diffusionRate = 0.2;
        this.diffusion = 0;
    }

    addPheromone(value = 0) {
        this.pheromone += value
    }

    getPheromone() {
        return this.pheromone
    }

    getDiffusionRate() {
        return this.diffusionRate
    }

    getDiffusion() {
        return this.diffusion
    }

    updateCell(cells, w, h) {
        // Code pour mettre à jour la phéromone en fonction du taux d'évaporation et de diffusion
        this.pheromone > 0 && (this.pheromone -= this.evaporationRate)

        var diffusion = 0;

        if (this.x - 1 > 0) {
            diffusion += (cells[this.x - 1][this.y].getPheromone() + cells[this.x - 1][this.y].getDiffusion())
        }

        if (this.y - 1 > 0) {
            diffusion += (cells[this.x][this.y - 1].getPheromone() + cells[this.x][this.y - 1].getDiffusion())
        }

        if (this.x + 1 < w) {
            diffusion += (cells[this.x + 1][this.y].getPheromone() + cells[this.x + 1][this.y].getDiffusion())
        }

        if (this.y + 1 < h) {
            diffusion += (cells[this.x][this.y + 1].getPheromone() + cells[this.x][this.y + 1].getDiffusion())
        }
        // console.log(diffusion * this.diffusionRate)

        this.diffusion = diffusion * this.diffusionRate
    }

}