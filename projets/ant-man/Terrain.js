class Terrain {
    constructor(width, heigth) {
        this.w = width;
        this.h = heigth
        this.terrain = document.getElementById("ant-canvas");
        this.ctx = this.terrain.getContext("2d");
        this.cells = []
    }

    createTerrain() {
        for (var i = 0; i < this.w; i++) {
            this.cells[i] = [];
            for (var j = 0; j < this.h; j++) {
                this.cells[i][j] = new Cell(i, j);
            }
        }
    }

    updateTerrain() {
        for (var i = 0; i < this.w; i++) {
            for (var j = 0; j < this.h; j++) {
                this.cells[i][j].updateCell(this.cells, this.w, this.h)
            }
        }
    }

    draw() {
        this.ctx.clearRect(0, 0, this.w, this.h);

        for (var i = 0; i < this.w; i++) {
            for (var j = 0; j < this.h; j++) {

                this.ctx.fillStyle = `rgba(0, 0, ${this.cells[i][j].getPheromone() + this.cells[i][j].getDiffusion()}, 1)`;
                this.ctx.fillRect(this.cells[i][j].x, this.cells[i][j].y, 1, 1);
            }
        }
    }

    testDiffusion() {
        this.cells[this.w / 2][this.h / 2].addPheromone(255)

    }
}