/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice1 :                                              #
#                                                                              #
#                                                                              #
# *****************************************************************************/

import prompts from "prompts"
import { sutom } from "./resource/sutom.js"

const word = sutom

const getFromUser = async (word) => {
    const response = await prompts({
        type: "text",
        name: "value",
        message: "essai",
        validate: value => value.length !== word.length ? `La taille du mot est incorrect` : true
    })
    return response
}



const compare = (letter, word) => {
    for (let index = 0; index < word.length; index++) {
        if (word[index] === letter) {
            return true
        }
    }
    return false
}


const game = async (word) => {
    console.log("Le mot a " + word.length + " lettres")
    for (let i = 0; i < 5; i++) {
        const temp = await getFromUser(word)
        const splittedWord = [...word]
        const splittedEntry = [...temp.value]

        splittedEntry.forEach((letter, i) => {
            if (splittedWord[i] === letter) {
                console.log(`la lettre ${letter} est bien placée`)
            } else {
                if (compare(letter, splittedWord)) {
                    console.log(`la lettre ${letter} est mal placée`)
                } else {
                    console.log(`la lettre ${letter} n'est pas dans le mot`)
                }
            }
        });
    }
}

game(word)