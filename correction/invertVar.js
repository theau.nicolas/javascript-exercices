/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : inverser valeur de la variable                                 #
#                                                                              #
#                                                                              #
# *****************************************************************************/

var a = 1;
var b = "A";
var c = a;

a = b;
b = c;
console.log("a = " + a, "b = " + b)