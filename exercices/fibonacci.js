// 0, 1

const fibonacci = (limit) => {
    let a = 0;
    let b = 1;
    let c = 0;

    const tab = []

    for (let i = 0; i < limit; i++) {
        c = a + b
        tab.push(a)
        a = b
        b = c
    }
    return tab
}

console.log(fibonacci(30))
