/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : Afficher liste de nombre                                       #
#                                                                              #
#                                                                              #
# *****************************************************************************/

function displayNum(limit) {
    for (let i = 0; i < limit; i++) {
        console.log(i + 1)
    }
}

displayNum(20)