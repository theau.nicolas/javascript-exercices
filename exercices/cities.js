/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : faire un algorytme de recherche de ville                       #
#               (en pensant à tout les cas)                                    #
#                                                                              #
#                                                                              #
# *****************************************************************************/

// const { default: levenshtein } = require("./levenschtein");
// const cities = require("./resource/cities.json")

import cities from "./resource/cities.json"
import levenshtein from "./levenschtein.js";
const entry = process.argv[2]

const filter = (str) => {
    const toRemove = [" ", "-"]
    toRemove.forEach(element => {
        str = str.replace(element, "")
    });
    return str
}


const searchByCity = (entry) => {
    const suggest = []
    cities.cities.forEach(element => {
        const match = filter(element.label).match(filter(entry))
        if (match && filter(entry).length < 9) {
            suggest.push({ ville: element.label, distance: 0, cp: element.zip_code })
        }
        else {
            const distance = levenshtein(filter(entry), filter(element.label))
            distance < 5 && suggest.push({ ville: element.label, distance, cp: element.zip_code })
        }
    });
    return (suggest.sort((a, b) => a.distance - b.distance).splice(0, 10))
}

const searchByZip = (entry) => {
    const suggest = []
    cities.cities.forEach(element => {
        element.zip_code.substring(0, entry.length) === entry && suggest.push({ ville: element.label, zip_code: element.zip_code })
    })
    return (suggest.sort((a, b) => a.zip_code - b.zip_code))
}

console.log(searchByZip(entry))

//executer : node cities.js "<nom de la ville>"

