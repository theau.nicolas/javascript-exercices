import invoices from "./resources/invoices.json" assert {type: 'json'}

function main() {

    var total = 0
    var totalFees = 0
    var totalHt = 0

    for (let i = 0; i < invoices.invoices.length; i++) {
        var totalInternalFees = Math.round((invoices.invoices[i].expenses.employeeSalaries + invoices.invoices[i].expenses.officeRent + invoices.invoices[i].expenses.equipmentCosts + invoices.invoices[i].expenses.marketingExpenses + invoices.invoices[i].expenses.otherExpenses) * 100) / 100
        var invoiceTotalHt = Math.round((invoices.invoices[i].total - ((invoices.invoices[i].total * 20) / 100)) * 100) / 100

        total += Math.round(invoices.invoices[i].total)
        totalHt += invoiceTotalHt
        totalFees += totalInternalFees
    }

    console.log("Chiffre d'affaire : " + total + "€")
    console.log("Chiffre d'affaire HT : " + totalHt + "€")
    console.log("bénéfice total : " + (totalHt - totalFees) + "€")
}

main()

