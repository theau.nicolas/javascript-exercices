// nombre / 1     nombre / lui-même
// /   %

function isPrime(nb) {
    var i = 2;
    while (i < nb) {
        if (nb % i === 0) {
            return false
        }
        i++;
    }
    return true
}

function generatePrime(limit) {
    var tab = []
    var i = 0;
    while (tab.length < limit) {
        if (isPrime(i)) {
            tab.push(i)
        }
        i++;
    }
    return tab
}

console.log(generatePrime(1000))