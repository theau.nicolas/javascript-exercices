const { exec } = require("child_process");


const wordTable = [
    { entry: "lyon", result: "lyon" },
    { entry: "Lyon", result: "lyon" },
    { entry: "st-etienne", result: "st etienne" },
    { entry: "'Saint Etienne'", result: "st etienne" },
    { entry: "Clermont-Ferrand", result: "clermont ferrand" },
    { entry: "'Clermont Fd'", result: "clermont ferrand" },
    { entry: "cl", result: "" },
    { entry: "cle", result: "clerac" },
]

wordTable.forEach(element => {
    exec(`node ../exercices/cities ${element.entry}`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        const out = stdout.replace(/(\r\n|\n|\r)/gm, "")
        console.log(`${out}: ${out === element.result ? "passed" : "failed"}`);
    })
});
