/***************************************************************************** #
#                                                                              #
#                                                                              #
#    Creator: Théau <theau.nicolas@cfanumeriqueoutremer.com>                   #
#                                                                              #
#    Created: 2013/11/18 13:37:42                                              #
#    Exercice : Trier un tableau avec une boucle ou en récursif                #
#                                                                              #
#                                                                              #
# *****************************************************************************/

function invertValue(tab, i) {
    var temp = tab[i]
    tab[i] = tab[i + 1]
    tab[i + 1] = temp
    return tab
}

function sortLoop(tab) {
    var i = 0;
    while (i < tab.length) {
        if (tab[i] > tab[i + 1]) {
            invertValue(tab, i)
            i = 0
        } else i++
    }
}

function sortRecursive(tab, i = 0) {

    if (i >= tab.length) {
        return tab
    }

    if (tab[i] > tab[i + 1]) {
        sortRecursive(invertValue(tab, i))
    }

    i++;
    sortRecursive(tab, i)
}


const tab = [3, 2, 4, 14, -1, 9, 3, 0]
// sortRecursive(tab)
sortLoop(tab)
console.log(tab)


